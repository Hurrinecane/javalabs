import java.io.*;
import java.util.ArrayList;
import java.util.logging.Logger;

public class Main {
    private static final Logger logger = Logger.getLogger(Main.class.getName());
    public static void main(String[] args) {
        task1();
        task2();
        task3();
    }

    //  Задание 1
    //Write a method public ArrayList<Double> readValues(String filename) throws ... that
    //reads a file containing floating-point numbers.
    // Throw appropriate exceptions if the file could not be opened or if some of the inputs are not
    //floating-point numbers.
    public static void task1() {
        try {
            ArrayList<Double> numbers = readValues("numbers.txt");
            logger.info(numbers.toString());
        }
        catch (IOException ex) {
            if (ex.getCause() != null) {
                logger.info(ex.getCause().getMessage());
            }
            else logger.info(ex.getMessage());
        }
        catch (NumberFormatException ex) {
            logger.info(ex.getMessage());
        }
    }
    public static ArrayList<Double> readValues(String filename)
            throws NumberFormatException, FileNotFoundException, IOException {
        ArrayList<Double> numbers = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            String line;
            while ((line = br.readLine()) != null) {
                double number = Double.parseDouble(line);
                numbers.add(number);
            }
        }
        return  numbers;
    }

    //  Задание 2
    // Write a method public double sumOfValues(String filename) throws ... that calls
    //the preceding method and returns the sum of the values in the file.
    //Propagate any exceptions to the caller.
    public static void task2() {
        try {
            double sum = sumOfValues("numbers.txt");
        }
        catch (Exception ex) {
            logger.info(ex.getMessage());
        }
    }

    public static double sumOfValues(String filename)
            throws NumberFormatException, FileNotFoundException, IOException {
        double sum = 0.0;
        ArrayList<Double> numbers = readValues(filename);
        for (double num : numbers) {
            sum += num;
        }
        return sum;
    }

    //  Задание 3
    // Write a program that calls the preceding method and prints the result.
    //Catch the exceptions and provide feedback to the user about any error
    //conditions.
    public static void task3() {
        try {
            double sum = sumOfValues("numbers.txt");
            logger.info("sum: " + sum);
        }
        catch (IOException ex) {
            if (ex.getCause() != null) {
                logger.info(ex.getCause().toString() + " :" + ex.getCause().getMessage());
            }
            else logger.info("IOException: " + ex.getMessage());
        }
        catch (NumberFormatException ex) {
            logger.info("NumberFormatException: " + ex.getMessage());
        }
    }
}

