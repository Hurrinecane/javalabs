import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {
    private static final Logger logger = Logger.getLogger(Main.class.getName());

    @Test
    void task1() {
        try {
            ArrayList<Double> numbers = Main.readValues("numbers.txt");
            assertEquals(
                    new ArrayList<>(List.of(1.21, 8.323, 9.9901, 6.09, 10.23)),
                    numbers
            );
        }
        catch (Exception ex) {
            logger.info(ex.getMessage());
        }
    }
    @Test
    void task1_2() {
            assertThrowsExactly(NumberFormatException.class, () -> Main.readValues("numbers_incorrect.txt"));
    }
    @Test
    void task1_3() {
        assertThrowsExactly(FileNotFoundException.class, () -> Main.readValues("numberss.txt"));
    }

    @Test
    void task2() {
        List<Double> originalValues = List.of(1.21, 8.323, 9.9901, 6.09, 10.23);
        double originalSum = originalValues.stream().reduce(0.0, (acc, cur) -> {
            acc += cur;
            return acc;
        });
        try {
            double sum = Main.sumOfValues("numbers.txt");
            assertEquals(originalSum, sum);
        }
        catch (Exception ex) {
            logger.info(ex.getMessage());
        }
    }

    @Test
    public static void task3() {
        try {
            double sum = Main.sumOfValues("numbers.txt");
            logger.info("sum: " + sum);
        }
        catch (IOException ex) {
            if (ex.getCause() != null) {
                logger.info(ex.getCause().toString() + " :" + ex.getCause().getMessage());
            }
            else logger.info("IOException: " + ex.getMessage());
        }
        catch (NumberFormatException ex) {
            logger.info("NumberFormatException: " + ex.getMessage());
        }
    }
}