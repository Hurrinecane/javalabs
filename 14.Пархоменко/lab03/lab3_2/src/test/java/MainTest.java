import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {
    private static final Logger logger = Logger.getLogger(MainTest.class.getName());
    @Test
    void task1() {
        ArrayList<AutoCloseable> pws = new ArrayList<>();
        try {
            PrintWriter ac1  =new PrintWriter("text1.txt");
            PrintWriter ac2 =new PrintWriter("text2.txt");
            AutoCloseable ac3 = new AutoCloseable() {
                @Override
                public void close() throws Exception {
                    throw new Exception("You have no permission");
                }
            };
            pws.add(ac1);
            pws.add(ac3);
            pws.add(ac2);
        }
        catch (IOException ex) {
            logger.info(ex.getMessage());
        }
        try {
            Main.closeAll(pws);
        }
        catch (Exception ex) {
            logger.info(ex.getMessage());
        }
    }

    @Test
    void task2() {
        ArrayList<Integer> test = new ArrayList<Integer>(List.of(1, 2, 3));
        ArrayList<Integer> originalMultipliedTest = new ArrayList<>(List.of(2, 4, 6));
        ArrayList<Integer> multipliedTest = Main.map(test, (el) -> el * 2);
        assertEquals(originalMultipliedTest, multipliedTest);
    }
}