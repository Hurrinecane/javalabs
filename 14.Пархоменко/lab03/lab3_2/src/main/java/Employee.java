public class Employee implements Comparable<Employee> {
    float salary;
    String name;

    @Override
    public int compareTo(Employee other) {
        return Float.compare(salary, other.salary);
    }
}
