import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.logging.Logger;

public class Main {
    private static final Logger logger = Logger.getLogger(Main.class.getName());
    public static void main(String[] args) {
        task1();
        task2();
        task3();
    }

    public static void task1() {
        ArrayList<AutoCloseable> pws = new ArrayList<>();
        try {
            PrintWriter ac1  =new PrintWriter("text1.txt");
            PrintWriter ac2 =new PrintWriter("text2.txt");
            AutoCloseable ac3 = new AutoCloseable() {
                @Override
                public void close() throws Exception {
                    throw new Exception();
                }
            };
            pws.add(ac1);
            pws.add(ac2);
            pws.add(ac3);
        }
        catch (IOException ex) {
            logger.info(ex.getMessage());
        }
        try {
            closeAll(pws);
        }
        catch (Exception ex) {
            logger.info(ex.getMessage());
        }
    }
    public static <T extends AutoCloseable> void closeAll(ArrayList<T> elems)
            throws Exception {
        Exception commonException = null;
        for (T elem : elems) {
            try {
                elem.close();
                logger.info("Elem: " + elem.toString() + " has been closed");
            }
            catch (Exception ex) {
                commonException = new Exception(ex);
                logger.info("Error closing elem: " + elem.toString());
            }
        }
        if (commonException != null) throw commonException;
    }

    public static void task2() {
        ArrayList<Integer> test = new ArrayList<Integer>(List.of(1, 2, 3));
        ArrayList<Integer> multipliedTest = map(test, (el) -> el * 2);
    }
    public static <T> ArrayList<T> map(ArrayList<T> list, Function<T, T> function) {
        ArrayList<T> newList = new ArrayList<T>();
        for (T elem: list) {
            newList.add(function.apply(elem));
        }
        return newList;
    }
    public static void task3() {
        //  Класс Employee
    }
}