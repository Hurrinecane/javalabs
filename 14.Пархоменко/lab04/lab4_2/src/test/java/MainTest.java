import org.junit.jupiter.api.Test;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {
    private static final Logger logger = Logger.getLogger(Main.class.getName());
    @Test
    void Task1_1() {
        ArrayList<ArrayList<String>> lists = new ArrayList<>(List.of(
                new ArrayList<String>(List.of("один", "два", "три")),
                new ArrayList<String>(List.of("четыре", "пять", "шесть")),
                new ArrayList<String>(List.of("семь", "восемь", "сорок два"))
        ));

        ArrayList<String> correctList = new ArrayList<>(List.of(
                "один", "два", "три",
                "четыре", "пять", "шесть",
                "семь", "восемь", "сорок два"
        ));

        // Первый метод
        Stream<ArrayList<String>> listsStream = lists.stream();
        ArrayList<String> list = Main.reduceConcat1(listsStream);
        assertEquals(correctList, list);
    }

    @Test
    void Task1_2() {
        ArrayList<ArrayList<String>> lists = new ArrayList<>(List.of(
                new ArrayList<String>(List.of("один", "два", "три")),
                new ArrayList<String>(List.of("четыре", "пять", "шесть")),
                new ArrayList<String>(List.of("семь", "восемь", "сорок два"))
        ));

        ArrayList<String> correctList = new ArrayList<>(List.of(
                "один", "два", "три",
                "четыре", "пять", "шесть",
                "семь", "восемь", "сорок два"
        ));

        // Второй метод
        Stream<ArrayList<String>> listsStream = lists.stream();
        ArrayList<String> list = Main.reduceConcat2(listsStream);
        assertEquals(correctList, list);
    }

    @Test
    void Task1_3() {
        ArrayList<ArrayList<String>> lists = new ArrayList<>(List.of(
                new ArrayList<String>(List.of("один", "два", "три")),
                new ArrayList<String>(List.of("четыре", "пять", "шесть")),
                new ArrayList<String>(List.of("семь", "восемь", "сорок два"))
        ));

        ArrayList<String> correctList = new ArrayList<>(List.of(
                "один", "два", "три",
                "четыре", "пять", "шесть",
                "семь", "восемь", "сорок два"
        ));

        // Третий метод
        Stream<ArrayList<String>> listsStream = lists.stream();
        ArrayList<String> list = Main.reduceConcat3(listsStream);
        assertEquals(correctList, list);
    }

    @Test
    void Task2() {
        List<Double> numbers = List.of(12.45, 15.55, 16.43, 2.7);
        double sum = 0.0;
        for (Double number : numbers) {
            sum += number;
        }
        Stream<Double> stream = numbers.stream();
        assertEquals(sum / numbers.size(), Main.getAverage(stream));
    }

    @Test
    void Task3() {
        long startTime = System.currentTimeMillis();
        BigInteger[] numbers = Main.getPrimeIntegersParallel();
        long endTime = System.currentTimeMillis();
        logger.info("Parallel time elapsed: " + (endTime-startTime));
        assertEquals(numbers.length, 500);

        startTime = System.currentTimeMillis();
        numbers = Main.getPrimeIntegers();
        endTime = System.currentTimeMillis();
        logger.info("Serial time elapsed: " + (endTime-startTime));
        assertEquals(numbers.length, 500);
    }
}