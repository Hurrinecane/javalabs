import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Stream;

public class Main {

    private static final Logger logger = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        //Task1();
        Task2();
        Task3();
    }

    public static void Task1() {
        Stream<ArrayList<String>> listsStream = Stream.of(
                new ArrayList<String>(List.of("один", "два", "три")),
                new ArrayList<String>(List.of("четыре", "пять", "шесть")),
                new ArrayList<String>(List.of("семь", "восемь", "сорок два"))
        );
        ArrayList<String> list = reduceConcat1(listsStream);
        list = reduceConcat2(listsStream);
        list = reduceConcat3(listsStream);
    }

    public static <T> ArrayList<T> reduceConcat1(Stream<ArrayList<T>> stream) {
        return stream.reduce((acc, cur) -> {
            acc.addAll(cur);
            return acc;
        }).orElse(new ArrayList<>());
    }

    public static <T> ArrayList<T> reduceConcat2(Stream<ArrayList<T>> stream) {
        return stream.reduce(new ArrayList<>(), (acc, cur) -> {
            acc.addAll(cur);
            return acc;
        });
    }

    public static <T> ArrayList<T> reduceConcat3(Stream<ArrayList<T>> stream) {
        return stream.reduce(new ArrayList<T>(), (acc, cur) -> {
            acc.addAll(cur);
            return acc;
        }, (acc, cur) -> {
            acc.addAll(cur);
            return acc;
        });
    }

    public static void Task2() {
        Stream<Double> stream = Stream.of(12.45, 15.55, 16.43, 2.7);
        logger.info(String.valueOf(getAverage(stream)));
    }

    public static double getAverage(Stream<Double> stream) {
        DoubleInfo result = stream.reduce(new DoubleInfo(0.0, 0),
                (acc, cur) -> new DoubleInfo(acc.sum + cur, ++acc.count),
                (acc, cur) -> new DoubleInfo(acc.sum + cur.sum, acc.count + cur.count)
        );
        return result.count > 0 ? result.sum / result.count : 0.0;
    }

    private static class DoubleInfo {
        int count = 0;
        double sum = 0;

        public DoubleInfo(double sum, int count) {
            this.sum = sum;
            this.count = count;
        }
    }

    public static void Task3() {
    }

    public static BigInteger[] getPrimeIntegers() {
        BigInteger startValue = BigInteger.valueOf(10).pow(50);
        return Stream.iterate(startValue,
                        val -> val.add(BigInteger.ONE)).
                filter(el -> el.toString().length() >= 50).
                filter(el -> el.isProbablePrime(10)).
                limit(500).toArray(BigInteger[]::new);
    }

    public static BigInteger[] getPrimeIntegersParallel() {
        BigInteger startValue = BigInteger.valueOf(10).pow(50);
        return Stream.iterate(startValue,
                        val -> val.add(BigInteger.ONE)).parallel().
                filter(el -> el.toString().length() >= 50).
                filter(el -> el.isProbablePrime(10)).
                limit(500).toArray(BigInteger[]::new);
    }
}
