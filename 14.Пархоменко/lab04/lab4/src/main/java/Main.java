import java.sql.Driver;
import java.util.*;
import java.util.function.IntFunction;
import java.util.logging.Logger;

public class Main {
    private static final Logger logger = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        List<Integer> list = getImmutableList(5);
        logger.info(list.get(3).toString());

        List<Integer> list2 = getImmutableList2(Integer::valueOf);
        logger.info(list2.get(100).toString());

        List<Integer> list3 = getImmutableList3(Integer::valueOf);
        list3.get(2);
        list3.get(3);
        list3.get(100);
        logger.info(list3.get(100).toString());
    }

    //14. Write a method that produces an immutable list view of the numbers
    //from 0 to n, without actually storing the numbers.
    public static List<Integer> getImmutableList(int numbersCount) {
        return new AbstractList<>() {
            private final int cap = numbersCount;
            @Override
            public Integer get(int i) {
                return (i >=0 && i < cap) ? i : null;
            }
            @Override
            public int size() {
                return cap;
            }
            @Override
            public Iterator<Integer> iterator() {
                return new Iterator<>() {
                    private int currentIndex = 0;
                    @Override
                    public boolean hasNext() {
                        return currentIndex < cap;
                    }
                    @Override
                    public Integer next() {
                        return currentIndex++;
                    }
                    @Override
                    public void remove() {
                        throw new UnsupportedOperationException();
                    }
                };
            }
        };
    }

    //15. Generalize the preceding exercise to an arbitrary IntFunction. Note that the
    //result is an infinite collection, so certain methods (such as size and toArray)
    //should throw an UnsupportedOperationException.
    public static <T> List<T> getImmutableList2(IntFunction<T> function) {
        return new AbstractList<>() {
            @Override
            public int size() throws UnsupportedOperationException {
                throw new UnsupportedOperationException();
            }
            @Override
            public Object[] toArray() throws UnsupportedOperationException {
                throw new UnsupportedOperationException();
            }
            @Override
            public T get(int index) {
                return function.apply(index);
            }
            @Override
            public Iterator<T> iterator() {
                return new Iterator<>() {
                    private int currentIndex = 0;
                    @Override
                    public boolean hasNext() {
                        return true;
                    }
                    @Override
                    public T next() {
                        return function.apply(currentIndex++);
                    }
                    @Override
                    public void remove() {
                        throw new UnsupportedOperationException();
                    }
                };
            }
        };
    }

    //16. Improve the implementation of the preceding exercise by caching the last
    //100 computed function values.
    public static <T> List<T> getImmutableList3(IntFunction<T> function) {
        return new AbstractList<>() {
            private final Cache<Integer, T> cache = new Cache<>(100);

            @Override
            public int size() throws UnsupportedOperationException {
                throw new UnsupportedOperationException();
            }

            @Override
            public Object[] toArray() throws UnsupportedOperationException {
                throw new UnsupportedOperationException();
            }

            @Override
            public T get(int index) {
                if (cache.containsKey(index))
                    return cache.get(index);
                T elem = function.apply(index);
                cache.put(index, elem);
                return function.apply(index);
            }
            @Override
            public Iterator<T> iterator() {
                return new Iterator<>() {
                    private int currentIndex = 0;
                    @Override
                    public boolean hasNext() {
                        return true;
                    }
                    @Override
                    public T next() {
                        return function.apply(currentIndex++);
                    }
                    @Override
                    public void remove() {
                        throw new UnsupportedOperationException();
                    }
                };
            }
        };
    }

    public static class Cache<K, V> extends LinkedHashMap<K, V> {
        private final int capacity;

        Cache(int capacity) {
            super();
            this.capacity = capacity;
        }

        @Override
        protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
            return size() > capacity;
        }
    }
}