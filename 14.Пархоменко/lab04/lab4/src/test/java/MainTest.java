import org.junit.jupiter.api.Test;
import java.util.*;
import java.util.logging.Logger;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {
    private static final Logger logger = Logger.getLogger(Main.class.getName());
    @Test
    void task1() {
        List<Integer> list = Main.getImmutableList(5);
        assertEquals(list.get(3), 3);
        assertEquals(list.get(0), 0);
        Iterator<Integer> it = list.iterator();
        int counter = 0;
        while (it.hasNext()) {
            Integer value = it.next();
            logger.info("Iterator step: " + counter + "; Value: " + value);
            assertEquals(counter++, value);
        }
    }

    @Test
    void task2() {
        List<Integer> list = Main.getImmutableList2(Integer::valueOf);
        assertEquals(list.get(100), 100);
        assertEquals(list.get(5), 5);
        assertThrowsExactly(UnsupportedOperationException.class, list::size);
        assertThrowsExactly(UnsupportedOperationException.class, list::toArray);
    }

    @Test
    void task3() {
        List<Integer> list = Main.getImmutableList3(Integer::valueOf);
        list.get(2);
        list.get(3);
        list.get(100);
        assertEquals(list.get(100), 100);
    }
}