package Exercises;

import java.util.List;
import java.util.RandomAccess;

// Implement a method public static void swap(List<?> list, int i, int j) that swaps elements in the usual way
// when the type of list implements the RandomAccess interface, and that minimizes the cost of visiting
// the positions at index i and j if it is not.

public class Ch7_5 {
    public static void swap(List<?> list, int i, int j) {
        if (i >= list.size() || j >= list.size()) {
            throw new IllegalArgumentException("out of range");
        }

        if (list instanceof RandomAccess) {
            System.out.println("Called randomAccessSwap");
            randomAccessSwap(list, i, j);
        } else {
            System.out.println("Called sequentialAccessSwap");
            sequentialAccessSwap(list, i, j);
        }
    }

    private static <E> void sequentialAccessSwap(List<E> list, int i, int j) {
        var iter = list.listIterator(i);
        E first = iter.next();
        for (var x = i + 1; x < j; x++) {
            iter.next();
        }
        E second = iter.next();
        iter.set(first);
        for (var x = j; x >= i; x--) {
            iter.previous();
        }
        iter.set(second);
    }

    private static <E> void randomAccessSwap(List<E> list, int i, int j) {
        list.set(i, list.set(j, list.get(i)));
    }
}
