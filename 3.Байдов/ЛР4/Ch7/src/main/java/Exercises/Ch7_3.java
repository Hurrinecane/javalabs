package Exercises;

import java.util.HashSet;
import java.util.Set;

// How do you compute the union, intersection, and difference of two sets,
// using just the methods of the Set interface and without using loops?

public class Ch7_3 {
    public static <E> Set<E> union(Set<E> first, Set<E> second) {
        var result = new HashSet<E>(first.size() + second.size());
        result.addAll(first);
        result.addAll(second);
        return result;
    }

    public static <E> Set<E> intersection(Set<E> first, Set<E> second) {
        var result = new HashSet<E>(first.size() + second.size());
        result.addAll(first);
        result.retainAll(second);
        return result;
    }

    public static <E> Set<E> difference(Set<E> first, Set<E> second) {
        var result = union(first, second);
        result.removeAll(intersection(first, second));
        return result;
    }
}
