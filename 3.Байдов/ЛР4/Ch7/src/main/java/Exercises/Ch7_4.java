package Exercises;

import java.util.ConcurrentModificationException;
import java.util.List;

public class Ch7_4 {

    //    Produce a situation that yields a ConcurrentModificationException.
    //    What can you do to avoid it?

    public static <E> void removeTargetFor(List<E> list, E target) throws ConcurrentModificationException {
        for (E e : list) {
            if (e.equals(target)) {
                list.remove(e); //throws ConcurrentModificationException
            }
        }
    }

    public static <E> void addTargetFor(List<E> list, E target) throws ConcurrentModificationException{
        for (E e : list) {
            list.add(target); //throws ConcurrentModificationException
        }
    }

    public static <E> void removeTargetCollection(List<E> list, E target) throws ConcurrentModificationException {
        list.remove(target);
    }
}
