package Exercises;

import com.sun.tools.javac.Main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class App {
    public static void main( String[] args ) {

        Logger logger = Logger.getLogger(Main.class.getName());

        List<String> list = new ArrayList(Arrays.asList("AAA", "BBB", "CCC"));

        logger.log(Level.INFO, list.toString());

        try{
            Ch7_4.removeTargetFor(list, "AAA");
            //Ch7_4.addTargetFor(list, "DDD");
            //Ch7_4.removeTargetCollection(list, "AAA");
        }catch (UnsupportedOperationException exception){
            logger.log(Level.INFO, exception.toString());
        }

        logger.log(Level.INFO, list.toString());

    }
}
