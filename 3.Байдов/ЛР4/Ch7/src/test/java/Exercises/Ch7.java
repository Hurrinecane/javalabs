package Exercises;

import java.util.Arrays;
import java.util.HashSet;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class Ch7 {

    // How do you compute the union, intersection, and difference of two sets,
    // using just the methods of the Set interface and without using loops?

    @org.junit.jupiter.api.Test
    void Test1() {

        var s1 = new HashSet<Integer>(Arrays.asList(1, 2, 3));
        var s2 = new HashSet<Integer>(Arrays.asList(2, 3, 4));

        assertEquals(new HashSet<Integer>(Arrays.asList(1, 2, 3, 4)), Ch7_3.union(s1, s2));

        assertEquals(new HashSet<Integer>(Arrays.asList(2, 3)), Ch7_3.intersection(s1, s2));

        assertEquals(new HashSet<Integer>(Arrays.asList(1, 4)), Ch7_3.difference(s1, s2));
        
    }
    // Implement a method public static void swap(List<?> list, int i, int j) that swaps elements in the usual way
    // when the type of list implements the RandomAccess interface, and that minimizes the cost of visiting
    // the positions at index i and j if it is not.

    @org.junit.jupiter.api.Test
    void Test2(){

        ArrayList<Integer> arrayList = new ArrayList<Integer>(Arrays.asList(1,2,3));
        Ch7_5.swap(arrayList,1,2);
        assertEquals(Arrays.asList(1,3,2), arrayList);

    }
}
