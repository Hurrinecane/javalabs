package Exercises;

import java.util.stream.Stream;

//  The codePoints method in Section 8.3, “The filter, map, and flatMap
//  Methods” (page 263) was a bit clumsy, first filling an array list and then
//  turning it into a stream. Write a stream-based version instead, using the
//  IntStream.iterate method to construct a finite stream of offsets, then
//  extract

public class Ch8_5 {
    public static Stream<String> stringToStream(String str) {
        int len = str.codePointCount(0, str.length());
        return Stream
                .iterate(0, prev -> prev < len, prev -> str.offsetByCodePoints(prev, 1))
                .map(offset -> str.substring(offset, str.offsetByCodePoints(offset, 1)));
    }
}
