package Exercises;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class App {
    public static void main(String[] args) {
        System.out.println("Exercises 8.4");
        Stream<Long> random = Ch8_4.random(25214903917L, 11L, (long) Math.pow(2, 48), 0L);
        System.out.println(random.limit(5).collect(Collectors.toList()));

        System.out.println("Exercises 8.5");
        Stream<String> offsets = Ch8_5.stringToStream("thunderstorm");
        System.out.println(Arrays.toString(offsets.toArray()));
    }
}
