package Exercises;

import java.util.stream.Stream;

//        Using Stream.iterate, make an infinite stream of random numbers—not by
//        calling Math.random but by directly implementing a linear congruential
//        generator. In such a generator, you start with x0 = seed and then produce
//        xn + 1 = (a xn +c) %m, for appropriate values of a, c, and m. You should implement a
//        method with parameters a, c, m, and seed that yields a Stream<Long>. Try
//        out a = 25214903917, c = 11, and m = 2⁴⁸.

public class Ch8_4 {
    public static Stream<Long> random(Long a, Long c, Long m, Long seed) {
        return Stream.iterate(seed, (prev) -> (a * prev + c) % m);
    }
}
