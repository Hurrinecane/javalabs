package Exercises;

import java.io.FileNotFoundException;

//        Write a method public double sumOfValues(String filename)
//        throws ... that calls the preceding method and returns the sum of the values
//        in      the file. Propagate any exceptions to the caller.

public class Ch5_2 {
    public double sumOfValues(String filename) throws FileNotFoundException,NumberFormatException,NullPointerException {
        var delegate = new Ch5_1();
        var data = delegate.readValues(filename);
        var result = 0.0;
        for (var d : data) {
            result += d;
        }
        return result;
    }
}
