package Exercises;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

//    Write a method public ArrayList<Double> readValues(String
//    filename) throws ... that reads a file containing floating-point numbers.
//    Throw appropriate exceptions if the file could not be opened or if some of the
//    inputs are not floating-point numbers.

public class Ch5_1 {
    public ArrayList<Double> readValues(String filename) throws FileNotFoundException, NumberFormatException,NullPointerException {
        var result = new ArrayList<Double>();
        var in = new Scanner(new File(filename));

        while(in.hasNext()){
                result.add(Double.parseDouble(in.next()));
        }

        in.close();
        return result;
    }
}
