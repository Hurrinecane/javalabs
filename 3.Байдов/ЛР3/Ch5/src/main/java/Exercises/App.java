package Exercises;

import com.sun.tools.javac.Main;

import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class App {

    static Logger logger;

    public static void main( String[] args ) {
        //numbers 2 23 5.4
        Ch5_3 delegate = new Ch5_3();
        delegate.method("numbers.txt");
        delegate.method("numbers.tx");
    }

}
