package Exercises;

import com.sun.tools.javac.Main;

import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Ch5_3 {

//    Write a program that calls the preceding method and prints the result.
//    Catch the exceptions and provide feedback to the user about any error conditions.

    public static Double method(String fileName){

        Logger LOGGER = Logger.getLogger(Main.class.getName());

        var app = new Ch5_2();
        Double sum = null;
        try {
            sum =  app.sumOfValues(fileName);
            LOGGER.log(Level.INFO, "Sum: " + sum);
        }catch ( FileNotFoundException e) {
            LOGGER.log(Level.INFO, String.format("File \"%s\" could not be found. ", fileName) + e.getMessage());
        }catch (NumberFormatException e){
            LOGGER.log(Level.INFO, String.format("File contain error value " + e.getMessage()));
        }catch (NullPointerException e){
            LOGGER.log(Level.INFO, String.format("Pathname is " + e.getMessage()));
        }

        return sum;
    }
}
