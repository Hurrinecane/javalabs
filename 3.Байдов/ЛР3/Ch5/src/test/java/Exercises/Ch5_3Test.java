package Exercises;

import static org.junit.jupiter.api.Assertions.*;

class Ch5_3Test {

    @org.junit.jupiter.api.Test
    void TestCh5_3FileNotFound(){
        assertNull(Ch5_3.method("numbers.tx"));
    }

    @org.junit.jupiter.api.Test
    void TestCh5_3NumberFormatException(){
        assertNull(Ch5_3.method("numbers1.txt"));
    }

    @org.junit.jupiter.api.Test
    void TestCh5_3EqualsNumbers(){
        assertEquals(30.4, Ch5_3.method("numbers.txt"));
    }

    @org.junit.jupiter.api.Test
    void TestCh5_3NullPointerException() {
        assertNull(Ch5_3.method(null));
    }
}