package Exercises;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class MainTests {

//    Implement a class Table<K, V> that manages an array list of Entry<K, V>
//    elements. Supply methods to get the value associated with a key, to put a value
//    for a key, and to remove a key.

    @org.junit.jupiter.api.Test
    void Test1(){
        System.out.println("Exercises 6.3");

        Table<String, Integer> ch6_3 = new Table<>();
        ch6_3.set("A", 1);
        ch6_3.set("B", 2);
        ch6_3.set("C", 3);

        assertEquals(3, ch6_3.get("C"));
        assertEquals(2, ch6_3.get("B"));
        assertNull(ch6_3.get(""));

    }

//    In the previous exercise, make Entry into a nested class. Should that class
//    be generic?

    @org.junit.jupiter.api.Test
    void Test2(){
        System.out.println("Exercises 6.4");

        Table2<Character, Double> ch6_4 = new Table2<>();
        ch6_4.set('A', 1.5);
        ch6_4.set('B', 2.5);
        ch6_4.set('C', 3.5);

        assertEquals(3.5, ch6_4.get('C'));
        assertEquals(2.5, ch6_4.get('B'));
        assertNull(ch6_4.get('D'));
    }

//    Consider this variant of the swap method where the array can be supplied with
//    varargs:
//
//    public static <T> T[] swap(int i, int j, T... values) {
//        T temp = values[i];
//        values[i] = values[j];
//        values[j] = temp;
//        return values;
//    }
//
//    Now have a look at the call
//
//    Double[] result = Arrays.swap(0, 1, 1.5, 2, 3);
//
//    What error message do you get? Now call
//
//    Double[] result = Arrays.<Double>swap(0, 1, 1.5, 2, 3);
//
//    Has the error message improved? What do you do to fix the problem?

    @org.junit.jupiter.api.Test
    void Test3(){
        System.out.println("Exercises 6.5");
        assertEquals(new Double[]{2.0, 1.5, 3.0}[0], Arrays.<Double>swap(0, 1, 1.5, 2.0, 3.0)[0]);

        //Double[] result = Arrays.swap(0, 1, 1.5,2,3);
        //Double[] result = Arrays.<Double>swap(0, 1, 1.5,2,3);

        //SOLUTION
        //Double[] result = Arrays.<Double>swap(0, 1, 1.5,2.0,3.0);
    }
}
