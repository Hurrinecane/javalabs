//The Class class has six methods that yield a string representation of the
//type represented by the Class object. How do they differ when applied to
//arrays, generic types, inner classes, and primitive types?

public class lab4_8 {
    public static void main(String[] args) {
        Product car = new Product("car", 12345);
        int[] arr = {1, 2, 3};

        Class carR = car.getClass();
        Class arrR = arr.getClass();

        System.out.println(carR.getName());
        System.out.println(carR.getSimpleName());
        System.out.println(carR.getCanonicalName());
        System.out.println(carR.getPackageName());
        System.out.println(carR.getTypeName());
        System.out.println(carR.toString());
        System.out.println("--------------------------");


        System.out.println(arrR.getName());
        System.out.println(arrR.getSimpleName());
        System.out.println(arrR.getCanonicalName());
        System.out.println(arrR.getPackageName());
        System.out.println(arrR.getTypeName());
        System.out.println(arrR.toString());
    }
}

class Product {
    private String name;
    private int price;

    Product(String name, int price) {
        this.name = name;
        this.price = price;
    }
}