//Define an enumeration type for the eight combinations of primary colors
//BLACK, RED, BLUE, GREEN, CYAN, MAGENTA, YELLOW, WHITE with methods getRed, getGreen,
//and getBlue.

public class ch4_7 {
    public static void main(String[] args) {
        System.out.println(Color.getRed());
        System.out.println(Color.getGreen());
        System.out.println(Color.getBlue());
    }

    enum Color {
        BLACK,
        RED,
        BLUE,
        GREEN,
        CYAN,
        MAGENTA,
        YELLOW,
        WHITE;

        static Color getRed() {
            return RED;
        }

        static Color getGreen() {
            return GREEN;
        }

        static Color getBlue() {
            return BLUE;
        }
    }
}
