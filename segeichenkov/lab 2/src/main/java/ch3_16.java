// Implement the RandomSequence in Section 3.9.1, “Local Classes” (page 129) as
// a nested class, outside the randomInts method.

//  private static Random generator = new Random();
//  public static IntSequence randomInts(int low, int high) {
//      class RandomSequence implements IntSequence {
//          public int next() { return low + generator.nextInt(high - low + 1); }
//          public boolean hasNext() { return true; }
//      }
//      return new RandomSequence();
//  }

import java.util.Random;

public class ch3_16 {
    public static void main(String[] args) {
        IntSequence randomizer = randomInts(5, 10);
        System.out.println( randomizer.next());
        System.out.println( randomizer.next());
        System.out.println( randomizer.next());
        System.out.println( randomizer.next());
        System.out.println( randomizer.next());
        System.out.println( randomizer.next());
    }

    private static Random generator = new Random();

    public static interface IntSequence {
        boolean hasNext();

        int next();
    }

    private static class RandomSequence implements IntSequence {
        private final int low;
        private final int high;

        public RandomSequence(int low, int high) {
            this.low = low;
            this.high = high;
        }

        @Override
        public boolean hasNext() {
            return true;
        }

        @Override
        public int next() {
            return this.low + generator.nextInt(this.high - this.low);
        }
    }

    public static IntSequence randomInts(int low, int high) {
        return new RandomSequence(low, high);
    }
}
