// Provide an interface Measurable with a method double getMeasure() that mea-
//sures an object in some way. Make Employee implement Measurable. Provide
//a method double average(Measurable[] objects) that computes the average
//measure. Use it to compute the average salary of an array of employees.

// Continue with the preceding exercise and provide a method Measurable
//largest(Measurable[] objects). Use it to find the name of the employee with
//the largest salary. Why do you need a cast?

public class ch3_1 {
    public static void main(String[] args) {
        Employee[] measurables = {
            new Employee(1, "Vlad"),
            new Employee(6, "Gena"),
            new Employee(3, "Vitya"),
        };

        System.out.println(average(measurables));
        System.out.println(largest(measurables).getName());
    }

    public static double average(Measurable[] objects) {
        double result = 0;

        for (Measurable i : objects) {
            result += i.getMeasure();
        }

        return result / objects.length;
    }

    public static Measurable largest(Measurable[] objects) {
        double max = 0;
        Measurable maxCostEmployee = objects[0];

        for (Measurable i : objects) {
            if(i.getMeasure() > max) {
                max = i.getMeasure();
                maxCostEmployee = i;
            }
        }

        return  maxCostEmployee;
    }
}

interface Measurable {
    double getMeasure();
    String getName();
}

class Employee implements Measurable {
    private double salary;
    private String name;

    Employee(double salary, String name) {
        this.salary = salary;
        this.name = name;
    }

    @Override
    public double getMeasure() {
        return this.salary;
    }

    @Override
    public String getName() {
        return this.name;
    }
}
