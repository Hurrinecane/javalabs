// Suppose that in Section 4.2.2, “The equals Method” (page 148), the
//Item.equals method uses an instanceof test. Implement DiscountedItem.equals so
//that it compares only the superclass if otherObject is an Item, but also in-
//cludes the discount if it is a DiscountedItem. Show that this method preserves
//symmetry but fails to be transitive—that is, find a combination of items
//and discounted items so that x.equals(y) and y.equals(z), but not x.equals(z).



//public class Item {
//private String description;
//private double price;
//...
//public boolean equals(Object otherObject) {
//// A quick test to see if the objects are identical
//if (this == otherObject) return true;
//// Must return false if the parameter is null
//if (otherObject == null) return false;
//// Check that otherObject is an Item
//if (getClass() != otherObject.getClass()) return false;
//// Test whether the instance variables have identical values
//Item other = (Item) otherObject;
//return Objects.equals(description, other.description)
//&& price == other.price;
//}
//public int hashCode() { ... } // See Section 4.2.3
//}

import java.util.Objects;

public class ch4_6 {
    public static void main(String[] args) {
        DiscountedItem x = new DiscountedItem("x", 5, 2);
        Item y = new Item("x", 5);
        Item z = new DiscountedItem("x", 5, 4);
        System.out.println(x.equals(y));
        System.out.println(y.equals(z));
        System.out.println(x.equals(z));
    }

    public static class Item {
        private String description;
        private double price;

        public Item(String description, double price) {
            this.description = description;
            this.price = price;
        }

        @Override
        public boolean equals(Object otherObject) {
            // A quick test to see if the objects are identical
            if (this == otherObject) return true;
            // Must return false if the parameter is null
            if (otherObject == null) return false;
            // Check that otherObject is an Item
            if (!(otherObject instanceof Item)) return false;
            // Test whether the instance variables have identical values
            var other = (Item) otherObject;
            return Objects.equals(description, other.description) && price == other.price;
        }

        @Override
        public int hashCode() {
            return Objects.hash(this.description, this.price);
        }

    }

    public static class DiscountedItem extends Item {
        private double discount;

        public DiscountedItem(String description, double price, double discount) {
            super(description, price);
            this.discount = discount;
        }

        @Override
        public boolean equals(Object otherObject) {
            if (!super.equals(otherObject)) return false;
            System.out.println(Item.class);
            System.out.println(otherObject.getClass());
            if (otherObject.getClass() == Item.class) {
                return super.equals(otherObject);
            }

            var other = (DiscountedItem) otherObject;
            System.out.println(this.discount);
            System.out.println(other.discount);

            return this.discount == other.discount;
        }

        @Override
        public int hashCode() {
            return Objects.hash(this.discount, super.hashCode());
        }
    }
}
