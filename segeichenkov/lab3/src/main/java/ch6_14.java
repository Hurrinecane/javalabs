import java.util.List;

//Implement an improved version of the closeAll method in Section 6.3,
//“Type Bounds” (page 210). Close all elements even if some of them throw
//an exception. In that case, throw an exception afterwards. If two or more
//calls throw an exception, chain them together.

public class ch6_14 {
    public static <T extends AutoCloseable> void closeAll(List<T> elems) throws Exception {
        Exception ex = null;
        for (T elem : elems) {
            try {
                elem.close();
            } catch (Exception e) {
                ex = new Exception(e.getMessage(), ex);
            }
        }
        if (ex != null) {
            throw ex;
        }
    }
}
