import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

//Write a method public ArrayList<Double> readValues(String filename) throws ... that
//reads a file containing floating-point numbers. Throw appropriate excep-
//tions if the file could not be opened or if some of the inputs are not
//floating-point numbers.



//Write a method public double sumOfValues(String filename) throws ... that calls
//the preceding method and returns the sum of the values in the file.
//Propagate any exceptions to the caller.



//Write a program that calls the preceding method and prints the result.
//Catch the exceptions and provide feedback to the user about any error
//conditions.

public class ch5_1 {
    public static void main(String[] args) {
        String path = new File("").getAbsolutePath();
        String fullPath = path + "/src/main/java/data.txt";

        try {
            System.out.println(readValues(fullPath));
        } catch (FileNotFoundException ex) {
            System.out.println("not found filename");
        } catch (IllegalArgumentException ex) {
            System.out.println(ex);
        } catch (IllegalStateException ex) {
            System.out.println(ex);
        }

        try {
            System.out.println(sumOfValues(fullPath));
        } catch (FileNotFoundException ex) {
            System.out.println("not found filename");
        } catch (IllegalArgumentException ex) {
            System.out.println(ex);
        } catch (IllegalStateException ex) {
            System.out.println(ex);
        }
    }

    static public ArrayList<Double> readValues(String filename) throws FileNotFoundException {
        if (filename == null || filename.isEmpty()) {
            throw new IllegalArgumentException("invalid filename");
        }
        var result = new ArrayList<Double>();
        var in = new Scanner(new File(filename));
        while (in.hasNext()) {
            if(in.hasNextDouble()) {
                result.add(in.nextDouble());
            } else {
                throw new IllegalStateException("invalid inputs");
            }
        }
        return result;
    }

    static public double sumOfValues(String filename) throws FileNotFoundException {
        var arr = readValues(filename);
        var result = 0.0;
        for (var d : arr) {
            result += d;
        }
        return result;
    }
}

