import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

//Implement a method map that receives an array list and a Function<T, R>
//object (see Chapter 3), and that returns an array list consisting of the
//results of applying the function to the given elements.

public class ch6_15 {
    public static <R, T> List<R> map(List<T> list, Function<T, R> func) {
        var result = new ArrayList<R>(list.size());
        for (T e : list) {
            result.add(func.apply(e));
        }
        return result;
    }
}
