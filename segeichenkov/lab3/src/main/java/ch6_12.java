import java.util.Collections;
import java.util.Comparator;
import java.util.List;

//Implement the following method that stores the smallest and largest
//element in elements in the result list:
//public static <T> void minmax(List<T> elements,
//Comparator<? super T> comp, List<? super T> result)
//Note the wildcard in the last parameter—any supertype of T will do to
//hold the result.

public class ch6_12 {
    public static <T> void minmax(List<T> elements,
                                  Comparator<? super T> comp,
                                  List<? super T> result) {
        result.add(Collections.min(elements, comp));
        result.add(Collections.max(elements, comp));
    }
}