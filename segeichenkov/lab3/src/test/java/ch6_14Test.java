import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ch6_14Test {
    @Test
    public void testCloseAllNoExceptions() {
        List<AutoCloseable> elems = List.of(() -> {
        }, () -> {
        }, () -> {
        });
    }

    @Test
    public void testCloseAllOneException() {
        var myException = new Exception("dont close");
        List<AutoCloseable> elems = List.of(() -> {
        }, () -> {
        }, () -> {
            throw myException;
        });
        Throwable throwable = assertThrows(Exception.class, () -> ch6_14.closeAll(elems));
        assertEquals(myException.getMessage(), throwable.getMessage());
        throwable = throwable.getCause();
        assertNull(throwable);
    }

    @Test
    public void testCloseAllExceptions() {
        var third = new Exception("third dont close");
        var second = new Exception("second dont close");
        var first = new Exception("first dont close");
        List<AutoCloseable> elems = List.of(() -> {
            throw first;
        }, () -> {
            throw second;
        }, () -> {
            throw third;
        });
        Throwable throwable = assertThrows(Exception.class, () -> ch6_14.closeAll(elems));
        assertEquals(third.getMessage(), throwable.getMessage());
        throwable = throwable.getCause();
        assertEquals(second.getMessage(), throwable.getMessage());
        throwable = throwable.getCause();
        assertEquals(first.getMessage(), throwable.getMessage());
        throwable = throwable.getCause();
        assertNull(throwable);
    }
}