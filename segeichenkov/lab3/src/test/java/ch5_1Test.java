import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

class ch5_1Test {
    @Test
    public void testReadValuesBadFilename() {
        var app = new ch5_1();
        var exception = assertThrows(IllegalArgumentException.class, () -> {
            app.readValues(null);
        });
        assertEquals("invalid filename", exception.getMessage());
        var exception2 = assertThrows(IllegalArgumentException.class, () -> {
            app.readValues("");
        });
        assertEquals("invalid filename", exception2.getMessage());
    }

    @Test
    public void testReadValues() throws IOException {
        String path = new File("").getAbsolutePath();
        String fullPath = path + "/src/main/java/data.txt";

        var expected = new ArrayList<>(Arrays.asList(2.1, 3.2, 4.0));

        var result = new ArrayList<Double>();
        var in = new Scanner(new File(fullPath));
        while (in.hasNext()) {
            if(in.hasNextDouble()) {
                result.add(in.nextDouble());
            } else {
                throw new IllegalStateException("invalid inputs");
            }
        }

        assertEquals(expected, result);
    }

    @Test
    public void testReadBadValues() throws IOException {
        String path = new File("").getAbsolutePath();
        String fullPath = path + "/src/main/java/data.txt";

        var app = new ch5_1();

        var exception = assertThrows(IllegalStateException.class, () -> {
            app.readValues(fullPath);
        });
        assertEquals("invalid inputs", exception.getMessage());
    }
}