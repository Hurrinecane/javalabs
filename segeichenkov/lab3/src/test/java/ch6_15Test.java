import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.*;

class ch6_15Test {
    @Test
    public void testMap() {
        Function<Integer, Integer> doubleNumber = t -> t * 2;
        assertEquals(List.of(10, 4, 0, 2), ch6_15.map(List.of(5, 2, 0, 1), doubleNumber));
        assertEquals(List.of(0, 2, 4, 0), ch6_15.map(List.of(0, 1, 2, 0), doubleNumber));

        Function<Integer, Boolean> isEven = t -> t % 2 == 0;
        assertEquals(List.of(true, true, true, true, false), ch6_15.map(List.of(100, 200, 8, 4, 101), isEven));
        assertEquals(List.of(false, false, false), ch6_15.map(List.of(1, 3, 5), isEven));
    }
}