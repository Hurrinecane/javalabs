import org.junit.jupiter.api.Test;

import java.io.IOException;

class ch8_17Test {
    @Test
    void checkTime() throws IOException {
        long startTime = 0;
        long endTime = 0;

        startTime = System.currentTimeMillis();
        ch8_17.serialFind();
        endTime = System.currentTimeMillis();
        System.out.println("Serial time : " + (endTime-startTime));

        startTime = System.currentTimeMillis();
        ch8_17.parallelFind();
        endTime = System.currentTimeMillis();
        System.out.println("Parallel time : " + (endTime-startTime));
    }

}