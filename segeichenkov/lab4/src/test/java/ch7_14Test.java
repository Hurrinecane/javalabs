import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.NoSuchElementException;


class ch7_14Test {
    @Test
    public void testImmutableListView() {
        var n = 100;
        var integerIterator = ch7_14.getImmutableList(n);
        for (var i = 0; i <= n; i++) {
            assertTrue(integerIterator.hasNext());
            assertEquals(i, (int) integerIterator.next());
        }
        assertFalse(integerIterator.hasNext());
        assertThrows(NoSuchElementException.class, () -> integerIterator.next());
    }
}