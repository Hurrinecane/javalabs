import org.junit.jupiter.api.Test;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.*;

class ch8_16Test {
    @Test
    void checkTime() {
        long endTime = 0;
        long startTime = 0;

        startTime = System.currentTimeMillis();
        BigInteger[] numbers = ch8_16.find500PrimesWith50DigitsParralel();
        endTime = System.currentTimeMillis();

        System.out.println("Parallel time : " + (endTime-startTime));
        assertEquals(numbers.length, 500);

        startTime = System.currentTimeMillis();
        numbers = ch8_16.find500PrimesWith50Digits();
        endTime = System.currentTimeMillis();

        System.out.println("Default time: " + (endTime-startTime));
        assertEquals(numbers.length, 500);
    }
}