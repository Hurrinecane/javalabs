import org.junit.jupiter.api.Test;

import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

class ch7_16Test {
    @Test
    void task3() {
        List<Integer> list = ch7_16.getImmutableListCached(Integer::valueOf);
        list.get(2);
        list.get(3);
        list.get(100);

        assertEquals(list.get(100), 100);
    }
}