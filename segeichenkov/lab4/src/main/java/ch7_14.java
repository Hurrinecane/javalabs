
//Write a method that produces an immutable list view of the numbers
//from 0 to n, without actually storing the numbers.

import java.util.Iterator;
import java.util.NoSuchElementException;

public class ch7_14 {
    public static void main(String[] args) {

    }

    public static Iterator<Integer> getImmutableList(int n) {
        return new Iterator<Integer>() {
            private int current;

            @Override
            public boolean hasNext() {
                return this.current <= n;
            }

            @Override
            public Integer next() {
                if (this.current > n) {
                    throw new NoSuchElementException("out of elements");
                }
                return this.current++;
            }
        };
    }
}
