import java.util.Iterator;
import java.util.function.IntFunction;

//Generalize the preceding exercise to an arbitrary IntFunction. Note that the
//result is an infinite collection, so certain methods (such as size and toArray)
//should throw an UnsupportedOperationException.

public class ch7_15 {
    public static void main(String[] args) {

    }

    public static Iterator<Integer> immutableListView(IntFunction<Integer> intFunction) {
        return new Iterator<Integer>() {
            private int current = 0;
            private IntFunction<Integer> func = intFunction;

            @Override
            public boolean hasNext() {
                return true;
            }

            @Override
            public Integer next() {
                this.current = this.func.apply(this.current);
                return this.current;
            }
        };
    }
}
