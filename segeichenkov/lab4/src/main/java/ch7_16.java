//Improve the implementation of the preceding exercise by caching the last
//100 computed function values.

import java.util.AbstractList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.IntFunction;

public class ch7_16 {
    public static void main(String[] args) {
        List<Double> list = getImmutableListCached(Double::valueOf);
        list.get(100);
        System.out.println(list.get(100));
    }

    public static <E> List<E> getImmutableListCached(IntFunction<E> fn) {
        return new AbstractList<>() {
            private Cache<Integer, E> cache = new Cache<>(100);

            @Override
            public E get(int index) {
                if (cache.containsKey(index)) {
                    System.out.printf("from cache %d\n", index);
                    return cache.get(index);
                }

                E ret = fn.apply(index);
                cache.put(index, ret);
                return ret;
            }

            @Override
            public int size() {
                throw new UnsupportedOperationException();
            }
        };
    }

    public static class Cache<K, V> extends LinkedHashMap<K, V> {
        private final int capacity;

        Cache(int capacity) {
            super();
            this.capacity = capacity;
        }

        @Override
        protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
            return size() > capacity;
        }
    }
}
