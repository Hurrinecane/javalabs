import java.math.BigInteger;
import java.util.Arrays;
import java.util.stream.Stream;

//Find 500 prime numbers with 50 decimal digits, using a parallel stream
//of BigInteger and the BigInter.isProbablePrime method. Is it any faster than
//using a serial stream?

public class ch8_16 {
    public static void main(String[] args) {
    }

    public static BigInteger[] find500PrimesWith50DigitsParralel() {
        BigInteger seed = BigInteger.valueOf((long) Math.pow(10, 50));

        return Stream
            .iterate(seed, prev -> prev.add(BigInteger.ONE))
            .parallel()
            .filter(b -> b.isProbablePrime(10))
            .limit(500)
            .toArray(BigInteger[]::new);
    }

    public static BigInteger[] find500PrimesWith50Digits() {
        BigInteger seed = BigInteger.valueOf((long) Math.pow(10, 50));

        return Stream
            .iterate(seed, prev -> prev.add(BigInteger.ONE))
            .filter(b -> b.isProbablePrime(10))
            .limit(500)
            .toArray(BigInteger[]::new);
    }
}
