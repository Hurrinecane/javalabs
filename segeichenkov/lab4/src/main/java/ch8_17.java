import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

//Find the 500 longest strings in War and Peace with a parallel stream. Is it
//any faster than using a serial stream?

public class ch8_17 {
    public static void main(String[] args) throws IOException {
    }

    public static void serialFind() throws IOException {
        String path = new File("").getAbsolutePath();
        String fullPath = path + "/src/main/java/data.txt";

        ArrayList<Integer> stringsLength = new ArrayList<Integer>();
        try {
            var lines = Files.readAllLines(Paths.get(fullPath));
            lines.stream().forEach(line -> {
                stringsLength.add(line.length());
            });

            Collections.sort(stringsLength);
            Collections.reverse(stringsLength);

//            for (int i = 0; i < 500; i++) {
//                System.out.println(stringsLength.get(i));
//            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void parallelFind() throws IOException {
        String path = new File("").getAbsolutePath();
        String fullPath = path + "/src/main/java/data.txt";

        ArrayList<Integer> stringsLength = new ArrayList<Integer>();
        try {
            var lines = Files.readAllLines(Paths.get(fullPath));
            lines.parallelStream().forEach(line -> {
                stringsLength.add(line.length());
            });

            Collections.sort(stringsLength);
            Collections.reverse(stringsLength);

//            for (int i = 0; i < 500; i++) {
//                System.out.println(stringsLength.get(i));
//            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}
