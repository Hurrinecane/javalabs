package Chapter7;

import junit.framework.TestCase;

import java.util.BitSet;
import java.util.HashSet;

public class Exercise1Test extends TestCase {

    public void testGetPrimeNumbers1() {
        HashSet<Integer> expectedPrimes = new HashSet<>(){{
            add(2);
            add(3);
            add(5);
            add(7);
        }};
        HashSet<Integer> actualPrimes = Exercise1.getPrimeNumbers1(10);

        assertEquals(expectedPrimes, actualPrimes);
    }

    public void testGetPrimeNumbers2() {
        BitSet expectedPrimes = new BitSet(){{
            set(2);
            set(3);
            set(5);
            set(7);
        }};
        BitSet actualPrimes = Exercise1.getPrimeNumbers2(10);

        assertEquals(expectedPrimes, actualPrimes);
    }
}