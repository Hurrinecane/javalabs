package Chapter7;

import junit.framework.TestCase;

import java.util.ArrayList;

public class Exercise2Test extends TestCase {

    private final ArrayList<String> actualStrings = new ArrayList<>(){{
        add("Test1");
        add("tEst2");
        add("test3");
        add("TEST4");
    }};
    private final ArrayList<String> expectedStrings = new ArrayList<>(){{
        add("TEST1");
        add("TEST2");
        add("TEST3");
        add("TEST4");
    }};
    private final Exercise2 object = new Exercise2(actualStrings);

    public void testIteratorOption() {
        assertEquals(expectedStrings, object.iteratorOption());
    }

    public void testLoopOption() {
        assertEquals(expectedStrings, object.loopOption());
    }

    public void testReplaceAllOption() {
        assertEquals(expectedStrings, object.replaceAllOption());
    }
}