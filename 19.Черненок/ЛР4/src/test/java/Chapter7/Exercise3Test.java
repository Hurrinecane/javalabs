package Chapter7;

import junit.framework.TestCase;

import java.util.HashSet;
import java.util.Set;

public class Exercise3Test extends TestCase {

    private Set<Integer> set1 = new HashSet<>(){{
        add(1);
        add(2);
        add(3);
        add(4);
        add(5);
    }};
    private Set<Integer> set2 = new HashSet<>(){{
        add(3);
        add(4);
        add(5);
        add(6);
        add(7);
    }};

    public void testUnion() {
        Set<Integer> expectedUnion = new HashSet<>(){{
            add(1);
            add(2);
            add(3);
            add(4);
            add(5);
            add(6);
            add(7);
        }};

        assertEquals(expectedUnion, Exercise3.union(set1, set2));
    }

    public void testIntersection() {
        Set<Integer> expectedIntersection = new HashSet<>(){{
            add(3);
            add(4);
            add(5);
        }};

        assertEquals(expectedIntersection, Exercise3.intersection(set1, set2));
    }

    public void testDifference() {
        Set<Integer> expectedDifference = new HashSet<>(){{
            add(1);
            add(2);
        }};

        assertEquals(expectedDifference, Exercise3.difference(set1, set2));
    }
}