package Chapter8;

import junit.framework.TestCase;

public class Exercise2Test extends TestCase {

    public void testMeasureStreamExecutionTime() {
        Exercise2 obj = new Exercise2();
        assertTrue(obj.measureStreamExecutionTime() >= obj.measureParallelStreamExecutionTime());
    }
}