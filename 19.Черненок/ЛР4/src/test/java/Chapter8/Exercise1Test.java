package Chapter8;

import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.List;

public class Exercise1Test extends TestCase {

    public void testGetLongNamesListAndGetAmountOfCalls() {
        List<String> names = new ArrayList<>() {{
            add("John"); add("Dexter"); add("Robert");
            add("Alex"); add("Bruce"); add("Marcus");
            add("Billy"); add("William"); add("Arnold");
        }};
        int shortestNameLength = 5;
        Exercise1 obj = new Exercise1(names, shortestNameLength);

        assertEquals(obj.getLongNamesList().size(), 5);
        assertEquals(obj.getAmountOfCalls(), 7);
    }
}