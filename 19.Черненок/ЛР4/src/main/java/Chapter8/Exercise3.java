package Chapter8;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class Exercise3 {
    private static final Logger LOGGER = LoggerFactory.getLogger(Exercise3.class);

    public static void run() {
        int[] values = {1, 4, 9, 16};
        intArrayToStream(values);
        intArrayToStreamOfPrimitives(values);
    }

    private static void intArrayToStream(int[] values) {
        List<int[]> list = Stream.of(values).toList();

        LOGGER.debug("Stream.of:");
        for (int[] array: list) {
            LOGGER.debug(Arrays.toString(array));
        }
    }

    private static void intArrayToStreamOfPrimitives(int[] values) {
        int[] array = Arrays.stream(values).toArray();

        LOGGER.debug("Arrays.Stream:");
        for (int item: array) {
            LOGGER.debug(String.valueOf(item));
        }
    }
}
