package Chapter8;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Exercise1();
        Exercise2();
        Exercise3();
    }

    private static void Exercise1() {
        List<String> names = new ArrayList<>() {{
            add("John"); add("Dexter"); add("Robert");
            add("Alex"); add("Bruce"); add("Marcus");
            add("Billy"); add("William"); add("Arnold");
        }};
        int shortestNameLength = 5;

        new Exercise1(names, shortestNameLength).getLongNamesList();
    }

    private static void Exercise2() {
        Exercise2 ex = new Exercise2();
        ex.measureStreamExecutionTime();
        ex.measureParallelStreamExecutionTime();
    }

    private static void Exercise3() {
        Exercise3.run();
    }
}
