package Chapter8;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Exercise2 {
    private static final Logger LOGGER = LoggerFactory.getLogger(Exercise2.class);
    private final ArrayList<BigInteger> integersList;

    public Exercise2() {
        BigInteger limit = new BigInteger("1000000");
        Stream<BigInteger> integers = Stream.iterate(
                BigInteger.ZERO,
                n -> n.compareTo(limit) < 0,
                n -> n.add(BigInteger.ONE)
        );
        integersList = new ArrayList<>(integers.toList());
        Collections.shuffle(integersList);
    }

    public long measureParallelStreamExecutionTime() {
        long executionTime = measureStreamExecutionTime(integersList.parallelStream());
        LOGGER.debug("ParallelStream execution time: " + executionTime + " ms");

        return executionTime;
    }

    public long measureStreamExecutionTime() {
        long executionTime = measureStreamExecutionTime(integersList.stream());
        LOGGER.debug("Stream execution time: " + executionTime + " ms");

        return executionTime;
    }

    private long measureStreamExecutionTime(Stream<BigInteger> stream) {
        long start = System.currentTimeMillis();
        stream.sorted().reduce(BigInteger::add);
        return System.currentTimeMillis() - start;
    }
}
