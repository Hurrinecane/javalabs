package Chapter8;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class Exercise1 {
    private final Logger LOGGER = LoggerFactory.getLogger(Exercise1.class);
    private final List<String> names;
    private int amountOfCalls = 0;
    private final int shortestNameLength;

    public Exercise1(List<String> names, int shortestNameLength) {
        this.names = names;
        this.shortestNameLength = shortestNameLength;
    }

    public List<String> getLongNamesList() {
        return names.stream()
                .filter(this::filterName)
                .limit(5)
                .toList();
    }

    private boolean filterName(String name) {
        amountOfCalls++;
        LOGGER.debug("filter method call with parameter '" + name + "' (len = " + name.length() + ")");
        return name.length() >= shortestNameLength;
    }

    public int getAmountOfCalls() {
        return amountOfCalls;
    }
}
