package Chapter7;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;

public class Exercise2 {
    private final ArrayList<String> strings;

    public Exercise2(ArrayList<String> strings) {
        this.strings = (ArrayList<String>) strings.clone();
    }

    public ArrayList<String> iteratorOption() {
        Iterator<String> iterator = strings.iterator();
        ArrayList<String> upperCasedStrings = new ArrayList<>();
        while (iterator.hasNext()) {
            upperCasedStrings.add(iterator.next().toUpperCase(Locale.ROOT));
        }

        return upperCasedStrings;
    }

    public ArrayList<String> loopOption() {
        ArrayList<String> upperCasedStrings = new ArrayList<>();
        for (String string : strings) {
            upperCasedStrings.add(string.toUpperCase());
        }

        return upperCasedStrings;
    }

    public ArrayList<String> replaceAllOption() {
        ArrayList<String> upperCasedStrings = (ArrayList<String>) strings.clone();
        upperCasedStrings.replaceAll(String::toUpperCase);

        return upperCasedStrings;
    }
}
