package Chapter7;

import java.util.HashSet;
import java.util.Set;

public class Exercise3 {
    public static Set<Integer> union(Set<Integer> first, Set<Integer> second) {
        Set<Integer> union = new HashSet<>();
        union.addAll(first);
        union.addAll(second);

        return union;
    }

    public static Set<Integer> intersection(Set<Integer> first, Set<Integer> second) {
        Set<Integer> intersection = new HashSet<>();
        Set<Integer> differenceFirst = difference(first, second);
        Set<Integer> differenceSecond = difference(second, first);
        intersection.addAll(first);
        intersection.addAll(second);
        intersection.removeAll(differenceFirst);
        intersection.removeAll(differenceSecond);

        return intersection;
    }

    public static Set<Integer> difference(Set<Integer> first, Set<Integer> second) {
        Set<Integer> difference = new HashSet<>();
        difference.addAll(first);
        difference.removeAll(second);

        return difference;
    }
}
