package Chapter7;

import java.util.BitSet;
import java.util.HashSet;
import java.util.Iterator;

//Sieve of Eratosthenes
public class Exercise1 {
    private static int smallestNumber;
    private static int limit;

    public static HashSet<Integer> getPrimeNumbers1(int limit) {
        Exercise1.limit = limit;
        HashSet<Integer> numbers = fillHashSet();
        smallestNumber = getFirstSmallest(numbers);
        while (canProceed()) {
            removeComposites(numbers);
            smallestNumber = getNextSmallest(numbers);
        }

        return numbers;
    }

    private static HashSet<Integer> fillHashSet() {
        HashSet<Integer> numbers = new HashSet<>();
        for (int i = 2; i <= limit; i++) {
            numbers.add(i);
        }

        return numbers;
    }

    private static Integer getFirstSmallest(HashSet<Integer> numbers) {
        return numbers.iterator().next();
    }

    private static void removeComposites(HashSet<Integer> numbers) {
        for (int i = 2 * smallestNumber; i <= limit; i += smallestNumber) {
            numbers.remove(i);
        }
    }

    private static boolean canProceed() {
        return smallestNumber*smallestNumber <= limit;
    }

    private static Integer getNextSmallest(HashSet<Integer> numbers) {
        int newSmallest = smallestNumber;
        Iterator<Integer> iterator = numbers.iterator();
        do {
            if (!iterator.hasNext())
                break;
            newSmallest = iterator.next();
        } while (newSmallest <= smallestNumber);

        return newSmallest;
    }



    public static BitSet getPrimeNumbers2(int limit) {
        Exercise1.limit = limit;
        BitSet numbers = fillBitSet();
        smallestNumber = getFirstSmallest(numbers);
        while (canProceedIteratingNumbers(numbers)) {
            removeComposites(numbers);
            smallestNumber = getNextSmallest(numbers);
        }

        return numbers;
    }

    private static BitSet fillBitSet() {
        BitSet numbers = new BitSet(limit);
        for (int i = 2; i <= limit; i++) {
            numbers.set(i);
        }

        return numbers;
    }

    private static int getFirstSmallest(BitSet numbers) {
        return numbers.nextSetBit(2);
    }

    private static void removeComposites(BitSet numbers) {
        for (int i = 2 * smallestNumber; i <= limit; i += smallestNumber) {
            numbers.set(i, false);
        }
    }

    private static boolean canProceedIteratingNumbers(BitSet numbers) {
        return numbers.previousSetBit(limit) != smallestNumber;
    }

    private static int getNextSmallest(BitSet numbers) {
        return numbers.nextSetBit(smallestNumber + 1);
    }
}
