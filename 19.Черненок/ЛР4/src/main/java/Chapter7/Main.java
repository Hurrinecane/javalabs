package Chapter7;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Main {
    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        Exercise1(10);
        Exercise2();
        Exercise3();
    }

    private static void Exercise1(int limit) {
        HashSetOption(limit);
        BitSetOption(limit);
    }

    private static void HashSetOption(int limit) {
        LOGGER.info("Exercise1. HashSet<Integer>");

        HashSet<Integer> primeNumbers = Exercise1.getPrimeNumbers1(limit);
        StringBuilder numbers = new StringBuilder();
        for (Integer primeNumber : primeNumbers) {
            numbers.append(primeNumber).append(" ");
        }
        LOGGER.debug(numbers.toString());
    }

    private static void BitSetOption(int limit) {
        LOGGER.info("Exercise1. BitSet");

        BitSet primeNumbers = Exercise1.getPrimeNumbers2(limit);
        LOGGER.debug(primeNumbers.toString());
    }



    private static void Exercise2() {
        ArrayList<String> strings = new ArrayList<>() {{
            add("test1");
            add("TEST2");
            add("TeST3");
            add("tESt4");
        }};
        Exercise2 obj = new Exercise2(strings);
        printAllOptions(obj);
    }

    private static void printAllOptions(Exercise2 obj) {
        LOGGER.info("Exercise 2. Iterator Option");
        printArrayList(obj.iteratorOption());

        LOGGER.info("Exercise 2. Loop Option");
        printArrayList(obj.loopOption());

        LOGGER.info("Exercise 2. replaceAll() Option");
        printArrayList(obj.replaceAllOption());
    }

    private static void printArrayList(ArrayList<String> strings) {
        StringBuilder output = new StringBuilder();
        for (String string: strings) {
            output.append(string).append(" ");
        }
        LOGGER.debug(output.toString());
    }



    private static void Exercise3() {
        Set<Integer> first = new HashSet<>(){{
            addAll(List.of(1,2,3,4,5));
        }};
        Set<Integer> second = new HashSet<>(){{
            addAll(List.of(3,4,5,6,7));
        }};
        printAllOperations(first, second);
    }

    private static void printAllOperations(Set<Integer> first, Set<Integer> second) {
        LOGGER.info("Exercise 3. Union");
        LOGGER.debug(Exercise3.union(first, second).toString());

        LOGGER.info("Exercise 3. Intersection");
        LOGGER.debug(Exercise3.intersection(first, second).toString());

        LOGGER.info("Exercise 3. Difference");
        LOGGER.debug(Exercise3.difference(first, second).toString());
    }
}
