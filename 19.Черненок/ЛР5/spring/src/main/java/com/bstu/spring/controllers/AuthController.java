package com.bstu.spring.controllers;

import com.bstu.spring.AuthResponse;
import com.bstu.spring.models.User;
import com.bstu.spring.services.UserService;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Controller
public class AuthController {
    @GetMapping(value = "/authentication")
    public String auth() {
        return "auth";
    }

    @PostMapping("/authentication/{action}")
    public ModelAndView auth(
            @RequestParam String username,
            @RequestParam String password,
            @PathVariable String action) throws IOException {
        User userCredentials = new User(username, getPasswordDigest(password));
        switch (action) {
            case "sign-in":
                return signIn(userCredentials);
            case "sign-up":
                return signUp(userCredentials);
            case "sign-out":
                return signOut();
            default:
                return new ModelAndView("auth");
        }
    }

    private String getPasswordDigest(String password) {
        String digest = null;
        try {
            byte[] passwordDigest = MessageDigest.getInstance("MD5").digest(password.getBytes(StandardCharsets.UTF_8));
            digest = new String(passwordDigest, StandardCharsets.UTF_8);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return digest;
    }

    private ModelAndView signIn(User userCredentials) throws IOException {
        UserService userService = new UserService();
        User user = userService.findUser(userCredentials.getUsername());
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();

        if (!userCredentials.getPassword().equals(user.getPassword())) {
            AuthResponse response = new AuthResponse(false, "Неверное имя пользователя или пароль");
            attributes.getResponse().getWriter().write(String.valueOf(JSONObject.wrap(response)));
            return new ModelAndView("authentication");
        } else {
            attributes.getRequest().getSession().setAttribute("username", user.getUsername());
            return new ModelAndView("redirect:/home");
        }
    }

    private ModelAndView signUp(User userCredentials) throws IOException {
        UserService userService = new UserService();
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();

        if (userService.findUser(userCredentials.getUsername()) != null) {
            AuthResponse response = new AuthResponse(false, "Пользователь с таким именем уже существует");
            attributes.getResponse().getWriter().write(String.valueOf(JSONObject.wrap(response)));
            return new ModelAndView("/authentication");
        } else {
            userService.saveUser(userCredentials);
            attributes.getRequest().getSession().setAttribute("username", userCredentials.getUsername());
            return new ModelAndView("redirect:/home");
        }
    }

    private ModelAndView signOut() {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        attributes.getRequest().getSession().removeAttribute("username");
        return new ModelAndView("redirect:/authentication");
    }
}
