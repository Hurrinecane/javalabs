package com.bstu.spring.services;

import com.bstu.spring.dao.UserDao;
import com.bstu.spring.models.User;

import java.util.List;

public class UserService {
    private final UserDao usersDao = new UserDao();

    public User findUser(int id) {
        return usersDao.findById(id);
    }

    public User findUser(String username) {
        return usersDao.findByName(username);
    }

    public void saveUser(User user) {
        usersDao.save(user);
    }

    public void deleteUser(User user) {
        usersDao.delete(user);
    }

    public void updateUser(User user) {
        usersDao.update(user);
    }

    public List<User> findAllUsers() {
        return usersDao.findAll();
    }
}