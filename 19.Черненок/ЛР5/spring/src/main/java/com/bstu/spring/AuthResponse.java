package com.bstu.spring;

public class AuthResponse {
    private boolean isValid;
    private String message;

    public AuthResponse(boolean isValid, String message) {
        this.isValid = isValid;
        this.message = message;
    }
}
