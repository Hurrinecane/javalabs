package com.bstu.spring.controllers;

import com.bstu.spring.models.User;
import com.bstu.spring.services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {
    @GetMapping(value = {"/", "/home"})
    public ModelAndView home() {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        if (attributes.getRequest().getSession(false) == null) {
            return new ModelAndView("redirect:/authentication");
        } else {
            String username = (String) attributes.getRequest().getSession().getAttribute("username");
            UserService service = new UserService();
            User user = service.findUser(username);
            return new ModelAndView("home").addObject(user);
        }
    }
}
