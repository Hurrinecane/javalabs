$(document).ready(() => {
    $('#button_sign-in').click(() => {
        sendAjax('sign-in')
    })
    $('#button_sign-up').click(() => {
        sendAjax('sign-up')
    })
})

function sendAjax(action) {
    $.ajax({
        type: 'POST',
        url: `authentication/${action}`,
        cache: false,
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8;',
        dataType: 'json',
        data: {
            username: $('input[name="username"]').val(),
            password: $('input[name="password"]').val()
        },
        success: (data) => {
            handleMessage(data)
        }
    })
}

function handleMessage(response) {
    if (response.redirect) {
        window.location.replace(response.redirect);
    } else {
        $('#username').addClass(response.is_valid ? 'valid' : 'invalid')
        $('.helper-text').attr('data-error', response.message)
    }

}