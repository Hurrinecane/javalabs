<%@ page contentType="text/html; charset=UTF-8" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ЛР5. Сервлеты</title>
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <!-- Compiled and minified JavaScript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
</head>
<body>
     <nav>
         <div class="nav-wrapper teal">
             <a href="#" class="brand-logo center">Саня, верни сотку!</a>
         </div>
     </nav>
    <div class="container z-depth-5" style="margin-top: 50px; width: 40%; padding: 2%;">
        <form method="post" action="authentication">
            <div class="input-field">
                <input id="username" name="username" type="text">
                <label for="username">Имя пользователя</label>
                <span class="helper-text">Не более 30 символов, плз</span>
            </div>
            <div class="input-field">
                <input id="password" name="password" type="password">
                <label for="password">Пароль</label>
            </div>
            <a class="waves-effect waves-light btn" id="button_sign-in">Войти</a>
            <a class="waves-effect waves-light btn" id="button_sign-up">Зарегистрироваться</a>
        </form>
    </div>
</body>

<script>
$(document).ready(() => {
    $('#button_sign-in').click(() => {
        sendAjax('sign-in')
    })
    $('#button_sign-up').click(() => {
        sendAjax('sign-up')
    })
})

function sendAjax(action) {
        $.ajax({
            type: 'POST',
            url: 'authentication',
            cache: false,
            contentType: 'application/x-www-form-urlencoded; charset=UTF-8;',
            dataType: 'json',
            data: {
                action: action,
                username: $('input[name="username"]').val(),
                password: $('input[name="password"]').val()
            },
            success: (data) => {
                handleMessage(data)
            }
        })
    }

function handleMessage(response) {
    if (response.redirect) {
        window.location.replace(response.redirect);
    } else {
        $('#username').addClass(response.is_valid ? 'valid' : 'invalid')
        $('.helper-text').attr('data-error', response.message)
    }

}
</script>
</html>