<%@ page contentType="text/html; charset=UTF-8" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ЛР5. Сервлеты</title>
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <!-- Compiled and minified JavaScript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
</head>
<body>
     <nav>
         <div class="nav-wrapper teal">
             <a href="#" class="brand-logo center">Саня, верни сотку!</a>
         </div>
     </nav>
    <div class="container valign-wrapper" style="margin-top: 50px; height: 80%;">
        <div class="row">
            <div class="col s12">
              <div class="card teal lighten-5 z-depth-5">
                <div class="card-content">
                  <span class="card-title"><%=(String)session.getAttribute("username")%></span>
                  <p>Приветствую, <%=(String)session.getAttribute("username")%>! Вам здесь не рады кста 💩</p>
                </div>
                <div class="card-action">
                  <a class="waves-effect waves-light btn" id="button_sign-out">Выйти</a>
                </div>
              </div>
            </div>
        </div>
    </div>
</body>

<script>
$(document).ready(() => {
    $('#button_sign-in').click(() => {
        sendAjax('sign-in')
    })
    $('#button_sign-up').click(() => {
        sendAjax('sign-up')
    })
    $('#button_sign-out').click(() => {
        sendAjax('sign-out')
    })
})

function sendAjax(action) {
    $.ajax({
        type: 'POST',
        url: 'authentication',
        cache: false,
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8;',
        dataType: 'json',
        data: {
            action: action,
            username: $('input[name="username"]').val(),
            password: $('input[name="password"]').val()
        },
        success: (data) => {
            handleMessage(data)
        }
    })
}

function handleMessage(response) {
    if (response.redirect) {
        window.location.replace(response.redirect);
    } else {
        $('#username').addClass(response.is_valid ? 'valid' : 'invalid')
        $('.helper-text').attr('data-error', response.message)
    }

}
</script>
</html>