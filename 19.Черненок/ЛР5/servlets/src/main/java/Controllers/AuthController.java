package Controllers;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;

import Models.User;
import org.json.*;

@WebServlet("/authentication")
public class AuthController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getSession().getAttribute("username") == null) {
            req.getRequestDispatcher("jsp/auth.jsp").forward(req, resp);
        } else {
            resp.sendRedirect(req.getContextPath()+"/home");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String username = req.getParameter("username");
        String password = getPasswordDigest(req.getParameter("password"));
        User user = new User(username, password);
        registerSqlDriver();

        switch (req.getParameter("action")) {
            case "sign-in":
                if (isValidUser(user)) {
                    performLogin(user, req);
                    redirectTo(req.getContextPath()+"/", resp);
                }
                else {
                    sendAuthFailed(resp, "Неверные имя пользователя или пароль");
                }
                break;
            case "sign-up":
                if (!userExists(user)) {
                    appendUserToDb(user);
                    performLogin(user, req);
                    redirectTo(req.getContextPath()+"/", resp);
                } else {
                    sendAuthFailed(resp, "Пользователь с таким именем уже существует");
                }
                break;
            case "sign-out":
                performLogout(req);
                redirectTo(req.getContextPath()+"/authentication", resp);
                break;
        }
    }

    private String getPasswordDigest(String password) {
        String digest = null;
        try {
            byte[] passwordDigest = MessageDigest.getInstance("MD5").digest(password.getBytes(StandardCharsets.UTF_8));
            digest = new String(passwordDigest, StandardCharsets.UTF_8);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return digest;
    }

    private void registerSqlDriver() {
        try {
            DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private boolean isValidUser(User user) {
        return userExists(user) && isCorrectCredentials(user);
    }

    private boolean userExists(User user) {
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/lab05", "root", "")) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select count(*) as rows_count from (select * from users where username='"+user.getUsername()+"') subquery");
            resultSet.next();
            return resultSet.getInt("rows_count") == 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    private boolean isCorrectCredentials(User user) {
        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/lab05", "root", "");
            Statement statement = connection.createStatement();ResultSet resultSet = statement.executeQuery("select * from users where username='"+user.getUsername()+"'");
            resultSet.next();
            return resultSet.getString("password").equals(user.getPassword());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    private void appendUserToDb(User user) {
        try (Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/lab05", "root", "")) {
            PreparedStatement statement = connection.prepareStatement("insert into users (username, password) values (?, ?);");
            statement.setString(1, user.getUsername());
            statement.setString(2, user.getPassword());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void sendAuthFailed(HttpServletResponse resp, String message) throws IOException {
        resp.setContentType("text/json");
        resp.setHeader("Cache-Control", "no-cache");
        resp.setCharacterEncoding("UTF-8");

        JSONObject response = new JSONObject();
        response.put("is_valid", false);
        response.put("message", message);
        response.write(resp.getWriter());
    }

    private void performLogin(User user, HttpServletRequest req) {
        req.getSession().setAttribute("username", user.getUsername());
    }

    private void performLogout(HttpServletRequest req) {
        req.getSession().removeAttribute("username");
        req.getSession().invalidate();
    }

    private void redirectTo(String url, HttpServletResponse resp) throws IOException {
        JSONObject response = new JSONObject();
        response.put("redirect", url);
        response.write(resp.getWriter());
    }
}
