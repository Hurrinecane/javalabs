package Chapter5;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.util.Scanner;

public class Exercise5 {
    public static void run() {
        Logger logger = LoggerFactory.getLogger(Exercise5.class);

        Scanner in = null;
        PrintWriter out = null;
        try {
            in = new Scanner(Paths.get("/usr/share/dict/words"));
            out = new PrintWriter("output.txt");
            while (in.hasNext())
                out.println(in.next().toLowerCase());
        } catch (IOException e) {
            logger.error(e.toString());
        } finally {
            in.close();
            out.close();
        }
    }
}
