package Chapter5;

public enum ErrorCodes
{
    OK,
    FILE_NOT_FOUND,
    ERROR_OPENING_FILE,
    INVALID_NUMBER_FORMAT
}