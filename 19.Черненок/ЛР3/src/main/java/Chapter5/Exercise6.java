package Chapter5;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

public class Exercise6 {
    public static void run() {
        BufferedReader in = null;
        try {
            in = Files.newBufferedReader(Path.of("somePath"), StandardCharsets.UTF_8);
            //Read from in
        } catch (IOException ex) {
            System.err.println("Caught IOException: " + ex.getMessage());
        } finally {
            if (in != null) {
                //in.close(); // Caution—might throw an exception
            }
        }
    }

    private static void optionA() {
        BufferedReader in = null;
        try {
            in = Files.newBufferedReader(Path.of("somePath"), StandardCharsets.UTF_8);
            //Read from in
        } catch (IOException ex) {
            System.err.println("Caught IOException: " + ex.getMessage());
        } finally {
            if (in != null) {
                try {
                    in.close(); // Caution—might throw an exception
                } catch (IOException e) {
                    System.err.println("Caught IOException: " + e.getMessage());
                }
            }
        }
    }

    private static void optionB() {
        BufferedReader in = null;
        try {
            try {
                in = Files.newBufferedReader(Path.of("somePath"), StandardCharsets.UTF_8);
                //Read from in
            } finally {
                assert in != null;
                in.close();
            }
        } catch (IOException ex) {
            System.err.println("Caught IOException: " + ex.getMessage());
        }
    }

    private static void optionC() {
        try (BufferedReader in = Files.newBufferedReader(Path.of("somePath"), StandardCharsets.UTF_8)) {
            //Read from in
            in.lines();
        } catch (IOException ex) {
            System.err.println("Caught IOException: " + ex.getMessage());
        }
    }
}
