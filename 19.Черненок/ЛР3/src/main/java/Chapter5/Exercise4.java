package Chapter5;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Scanner;

class Exercise4
{
    public static void run() {
        Logger logger = LoggerFactory.getLogger(Exercise4.class);

        String filename = "DoubleValues.txt";
        Object[] response = readValues(filename);
        switch ((ErrorCodes)response[0]) {
            case FILE_NOT_FOUND -> logger.error("File "+filename+" was not found...");
            case INVALID_NUMBER_FORMAT -> logger.error("File "+filename+" contains number of an invalid format...");
            case ERROR_OPENING_FILE -> logger.error("Unable to open file "+filename);
            default -> logger.info("File "+filename+" contains the following values: "+response[1].toString());
        }
    }

    private static Object[] readValues(String filename) {
        Object[] response = new Object[2];
        response[0] = ErrorCodes.OK;

        Scanner scanner;
        try {
            scanner = new Scanner(new File(Exercise4.class.getClassLoader().getResource(filename).toURI()));
        } catch (FileNotFoundException e) {
            response[0] = ErrorCodes.FILE_NOT_FOUND;
            return response;
        } catch (URISyntaxException e) {
            response[0] = ErrorCodes.ERROR_OPENING_FILE;
            return response;
        }
        scanner.useDelimiter(" ");

        ArrayList<Double> values = new ArrayList<>();
        while (scanner.hasNext()) {
            try {
                Double parsedValue = Double.parseDouble(scanner.next());
                values.add(parsedValue);
            } catch (NumberFormatException e) {
                response[0] = ErrorCodes.INVALID_NUMBER_FORMAT;
                return response;
            }
        }
        response[1] = values;

        return response;
    }
}