package Chapter6;

import java.lang.reflect.Array;

public class Exercise20 {
    @SafeVarargs
    public static <T> T[] repeat(int n, T... objs) {
        @SuppressWarnings("unchecked") T[] result =
                (T[]) Array.newInstance(objs.getClass().getComponentType(), n * objs.length);
        for (var i = 0; i < n; i++) {
            System.arraycopy(objs, 0, result, i * objs.length, objs.length);
        }
        return result;
    }
}
