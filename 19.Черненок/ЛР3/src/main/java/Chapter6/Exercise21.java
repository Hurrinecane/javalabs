package Chapter6;

import java.util.Collections;

public class Exercise21 {
    @SafeVarargs
    public static <T> T[] construct(int size, T... emptyValue) {
        return Collections.nCopies(size, null).toArray(emptyValue);
    }
}
